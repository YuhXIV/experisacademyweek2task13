﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment
{
    class Controller
    {
        public Controller()
        {
            ChaosArray<int> chaosInt = new ChaosArray<int>();
            Console.WriteLine("Inserting randomly....");
            Insert(chaosInt);
            Console.WriteLine($"\nResult:  {chaosInt.ToString()}");
            Console.WriteLine("Index:  [0 1 2 3 4 5 6 7 8 9] \n\n");

            Console.WriteLine("Retriving a random item...");
            int retrieve = chaosInt.Retrieve();
            if(retrieve != default)
            {
                Console.WriteLine($"Retrieved item: {retrieve}");
            }

            Console.WriteLine("\n");

            ChaosArray<string> chaosString = new ChaosArray<string>();
            Console.WriteLine("Inserting randomly....");
            Insert(chaosString);
            Console.WriteLine($"\nResult:  {chaosString.ToString()}");
            Console.WriteLine("Index:  [0 1 2 3 4 5 6 7 8 9] \n\n");

            Console.WriteLine("Retriving a random item...");
            string retrieveString = chaosString.Retrieve();
            if (retrieve != default)
            {
                Console.WriteLine($"Retrieved item: {retrieveString}");
            }
        }
        /// <summary>
        /// Insert a random integer in the array
        /// </summary>
        /// <param name="chaos"></param>
        public void Insert(ChaosArray<int> chaos)
        {
            Random rand = new Random();
            for (int i = 0; i < 5; i++)
            {
                int tmp = rand.Next(0, chaos.Chaos.Length);
                chaos.Add(tmp);
            }
        }
        public void Insert(ChaosArray<string> chaos)
        {
            Random rand = new Random();
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < 5; i++)
            {
                while (builder.Length <i)
                {
                    builder.Append(rand.Next(1,10).ToString());
                }
                chaos.Add(builder.ToString());
            }
        }
    }
}
