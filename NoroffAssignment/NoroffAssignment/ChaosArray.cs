﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace NoroffAssignment
{
    class ChaosArray<T> : IChaosArray<T>
    {
        public ChaosArray()
        {
            Chaos = new T[10];
        }
        public T[] Chaos { get; set; }

        /// <summary>
        /// Add function of the Generic list class. This add function is chaos... Adding in random place
        /// </summary>
        /// <param name="item"></param>
        public void Add(T item)
        {
            Random rand = new Random();
            int index = rand.Next(1, Chaos.Length);
            Console.WriteLine($"Inserting: {item.ToString()} at {index}...");
            try
            {
                if (Chaos[index] == null ||Chaos[index].Equals(default(T)))
                {
                    Chaos[index] = item;
                    Console.WriteLine($"Successfully inserted {item.ToString()}\n");
                } else
                {
                    throw new Exception("Index already occupied");
                }
            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        /// <summary>
        /// Retrieve a item from a random index, if there is an object there.. return it..
        /// if it is default, return default.
        /// </summary>
        /// <returns></returns>
        public T Retrieve()
        {
            Random rand = new Random();
            int index = rand.Next(0, Chaos.Length);
            Console.WriteLine($"Retriving item at index: {index}...");
            try
            {
                if(Chaos[index] == null || Chaos[index].Equals(default(T)))
                {
                    throw new Exception("Nothing to retrieve");
                } else
                {
                    return Chaos.ElementAt(index);
                }
            } catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return default;
        }

        /// <summary>
        /// Return a string of all value in the object in the array.
        /// </summary>
        /// <returns></returns>
        public string ToString()
        {
            string text = "";
            foreach (T type in Chaos)
            {
                if(type == null)
                {
                    text += "null ";
                } else
                {
                    text += $"{type.ToString()} ";
                }
                
            }
            return text;
        }
    }
}

