﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment
{
    interface IChaosArray<T>
    {
        void Add(T item);
        T Retrieve();
    }
}
